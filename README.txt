To use program:
  1. Type "make" in terminal
  2. ./genDB to read the records from the file and generate a "database"
  3. ./search to search the records in the generated "database"

What has been done?

  Two programs have been implemented, one which generates a data file based on an input file that contains the records and another program which allows for a binary search through all the data file.

Limitations:
  The header of the data file can only be up to 200 characters/bytes long.
    - This was mostly done so for a safe fgets, but I probably could have
      wrote the file length in the header file and read it in when creating
    - All fields are limited to 200 bytes long, again this done for fgets
    - Not allowed to have varying number of fields. i.e One record can't have
      6 fields and another only 2
      
What improvements could be made?
  - Simply allowing for fields/header to be longer than 200 bytes would be better,
    but could allow for malicious attacks.
  - A more modularized search approach. Compared to my genData.c the search
    program looks a bit messy since 95% of the code is in main
  - Automatically sorting of the records, instead of manually doing it in the
    input file. This would be possible because I have a linked list implemented.
    
Testing Done:
  - blackbox testing with random strings
  - whitebox testing with strategically inputted strings
    - some of these included records near the end, and multiple incorrect 
      records followed by a record that was in the file
  - modular/integration testing mainly for genData.c since it has complex
    structures and multiple functions
  - caught quite a few bugs, the big ones being infinite loops and memory
    corruption
