/*
    Filename: searchData.c
    Description: Allows search in output file from genData.c
                Can Search based on LastNameFirstName
                e.g BAIRDBEN
    Author: Benjamin Baird
    Last Modified: Feb 4, 2016
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// Field values
typedef enum {
  LNAME,
  FNAME,
  ADR,
  CITY,
  PROV,
  POSTAL
}fields;

int printRecord(FILE *dataFile, char *name) {
  int offset = (strlen(name) + 2) * -1;
  int numFields = 6;  // Would be passed through as an argument
  int fieldCounter = 0;
  char *buffer = calloc(201, sizeof(char));
  char *letter = malloc(sizeof(char)*2);
  fseek(dataFile, offset, SEEK_CUR);  

  printf("**** Found Records Fields **** \n");
  while ( fgets(letter,2, dataFile) && (fieldCounter < numFields) ) {

    // Prints individual fields
    if ( strcmp(letter,"|") == 0 ) {
      switch (fieldCounter) {
        case(LNAME):
          printf("Last_Name: %s\n", buffer);
          buffer = strcpy(buffer, "");
          break;
        case( FNAME ):
          printf("First_Name: %s\n", buffer);
          buffer = strcpy(buffer, "");
          break;
        case( ADR ):
          printf("Street_Address: %s\n", buffer);   
          buffer = strcpy(buffer, "");
          break;
        case( CITY ):
          printf("City_Name: %s\n", buffer);          
          buffer = strcpy(buffer, "");
          break;
        case ( PROV ):
          printf("Province: %s\n", buffer);
          buffer = strcpy(buffer, "");
          break;
        case ( POSTAL ):
          printf("Postal_Code: %s\n", buffer); 
          buffer = strcpy(buffer, "");
          break;
      }
      fieldCounter++;
      continue;
    }
    buffer = strcat(buffer,letter);
  }
  printf("*******************************\n");

  free(buffer);
  free(letter);
  return 0;
}


int main (int argc, char *argv[] ) {
  char *query = malloc( sizeof(char)*201 );
  FILE *indexFile = fopen("index", "r");
  if (indexFile == NULL) {
    printf("Could not load index file\n");
    return 1;
  }
  //Load index into memory
  int numRecords = 0;
  int *index = malloc(sizeof(int)*(numRecords+1));
  char recordLocation[200] = "";
  int charLen = 0;
  char letter = '0';
  
  /* Read character by character until a space is found, then extract the number
     from the string */
  while ( (letter = fgetc (indexFile)) != EOF ){
    if (letter == ' ') {
      numRecords++;
      int *temp = realloc( index, sizeof(int)*numRecords );
      if (temp == NULL) {
        printf("Error allocating memory!\n");
        return 1;
      }      
      index = temp;

      // Extracting the number from string
      int startPos = 0;
      sscanf(recordLocation,"%d",&startPos);
      index[numRecords-1] = startPos;
      charLen = 0;
      strcpy(recordLocation, "");

    } else {
      recordLocation[charLen] = letter;
      charLen++;
    }
  }

  FILE *dataFile = fopen("database.txt", "r");
  if (dataFile == NULL) {
    printf("Could not load database.txt\n");
    return 1;
  }
  fscanf(dataFile, "%*s");  // Skip over the header
  int recordsLocation = ftell(dataFile);  // Location of records after header    
  
  while (1) {
    printf("Enter '~~' to quit\n");
    printf("Enter the record you would like to search for(LASTNAME FIRSTNAME):\n"
           "You must enter the names in ALL CAPITALS\n");
    scanf("%s", query);

    // Remove the newline    
    if ( query[ strlen(query) - 1 ] == '\n' ) {
      query [strlen(query) - 1] = '\0';
    }
    
    // Stop searching
    if (strcmp(query, "~~") == 0) {
      break;
    }
  
    // Start searching
    char buffer = ' ';
    int middle = numRecords / 2;
    int found = 0;
    int fieldsFound = 0;
    int numFields = 2; // LAST|FIRST| -> 2 deliminators/fields
    int upOrDown = 0;
    int newPos = index[middle]; //Where the next record is
    int matchFound = 0;
    fseek(dataFile, recordsLocation, SEEK_SET);
    int min = 0;
    int max = numRecords;

    /*************************
        Binary search until record is (not) found
        Gets indexes, compares to middle index and sees if value is higher or
          lower.
        Focuses on the bottom/top half
        Repeat
     *************************/    
    while (!found) {
      char *fileString = calloc (201, sizeof(char) );
      fileString = strcpy(fileString, "");
      fieldsFound = 0;

      //index[] + 1 since starts at deliminator
      if (fseek(dataFile, newPos + 1 ,SEEK_CUR) != 0) {
        printf ("Error seeking through file\n");
        free(fileString);
        free(dataFile);
        free(query);
        free(index);
        fclose(indexFile);        
        return 1;
      }

      // Read 1 byte at a time and compare the bytes
      while ( buffer != EOF && numFields > fieldsFound) {
        if (fread (&buffer, 1, 1,dataFile) != 1) {
          printf("Error reading at new seek location\n");
          free(fileString);
          free(dataFile);
          free(query);
          free(index);
          fclose(indexFile);
          return 1;
        }
    
        // Check for deliminators
        if (buffer == '|') {
          fieldsFound++;
          
          // If 2 found and not found a match, try another record
          if (fieldsFound == numFields) {
            // Do a final check on the string
            upOrDown = strcmp(query, fileString);
            if (  upOrDown == 0 ) {
              printf("~~~~~ Found a matching record ~~~~~\n");
              printRecord(dataFile, fileString); 
              matchFound = 1;
            }
            break;
          }
        } else {
        
         // Create the string from buffer's characters
          buffer = toupper(buffer);
          //Append character in buffer to the string
          int stringLen = strlen(fileString);
          fileString = realloc(fileString, sizeof(char)*stringLen+2);
          fileString[stringLen] = buffer;
        }
      }
      
      if (matchFound) {
        free(fileString);
        break;
      }
      
      // Checks if the search has reached an end
      if ( min == max || min == middle || max == middle) {
        printf("~~~~~ No Records founds :( ~~~~~\n");      
        free(fileString);
        break;
      }
      
      // Checks whether to move the cursor forward or backwards from middle point
      if ( upOrDown < 0 ) {
        // BinarySearch moving down
        max = middle;
        newPos = (index[middle] - index[middle/2] + strlen(fileString) + 3) * -1;
        middle /= 2;
        
      } else if (upOrDown > 0 ) {
        // BinarySearch moving up
        min = middle + 1;
        int newLocation = middle + (numRecords - middle)/2;
        newPos = index[newLocation] - index[middle] - strlen(fileString) - 3;
        middle = newLocation;
      }

      free(fileString);
    }

  }

  free(query);
  free(index);
  fclose(indexFile);
  fclose(dataFile);
  return 0;
}
