/*
  filename: bairdb_a1.c
  Description: Creates the data file of contact information from input.txt
               input.txt is already sorted by LastName, FirstName
  Last Updated: Feb 4, 2016
  Author: Benjamin Baird
  Limitations: Header record can only be 200bytes long
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Field deliminator
const char deliminator [1] = "|";

// Field values
typedef enum {
  LNAME,
  FNAME,
  ADR,
  CITY,
  PROV,
  POSTAL
}fields;

// Record structure
typedef struct EMPLOYEE{
  char * Last_Name;
  char * First_Name;
  char * Street_Address;
  char * City_Name;
  char * Province;
  char * Postal_Code;
  struct EMPLOYEE *nextEmployee;
}EMPLOYEE;

typedef struct {
  EMPLOYEE *head;
  int numEmployees;
  int numFields;
}DATABASE;

// Creates and allocates memory for a new record
void * createEmployee () {
  EMPLOYEE *newRecord = malloc(sizeof(EMPLOYEE));
  newRecord->nextEmployee = NULL;
  return newRecord;
}

// Modifies the data of a field
void insertField( EMPLOYEE *record, char *data, int field) {
  // Append deliminator to the data
  char *delimedData = malloc(sizeof(char*) * strlen(data) + 2);
  strcpy(delimedData, data);
  strcat(delimedData, deliminator);
  
  // Do actual insertion of the data with deliminator to the field
  switch(field){
    case(LNAME):
      record->Last_Name = malloc( sizeof(char)*strlen(delimedData)+1 );
      strcpy( record->Last_Name, delimedData );
      break;
    case (FNAME):
      record->First_Name = malloc( sizeof(char)*strlen(delimedData)+1 );
      strcpy( record->First_Name, delimedData );
      break;
    case ( ADR ):
      record->Street_Address = malloc( sizeof(char)*strlen(delimedData)+1 );
      strcpy( record->Street_Address, delimedData );
      break;
    case ( CITY ):
      record->City_Name = malloc( sizeof(char)*strlen(delimedData)+1 );
      strcpy( record->City_Name, delimedData );
      break;
    case ( PROV ):
      record->Province= malloc( sizeof(char)*strlen(delimedData)+1 );
      strcpy( record->Province, delimedData );                      
      break;
    case ( POSTAL ):
      record->Postal_Code = malloc( sizeof(char)*strlen(delimedData)+1 );
      strcpy( record->Postal_Code, delimedData );
      break;  
  }
  free (delimedData);
}


int printEmployee (EMPLOYEE *record, FILE *outFile) {
  fprintf(outFile,"%s",record->Last_Name);
  fprintf(outFile, "%s", record->First_Name);
  fprintf(outFile, "%s", record->Street_Address);
  fprintf(outFile, "%s", record->City_Name);
  fprintf(outFile, "%s", record->Province);
  fprintf(outFile, "%s", record->Postal_Code);
  return 0;
}

void freeEmployee (EMPLOYEE *record) {
  
  free( record->Postal_Code );
  free( record->Province );
  free( record->City_Name );
  free( record->Street_Address );
  free( record->First_Name );
  free( record->Last_Name );
  free(record);
  return;
}

void freeDatabase (DATABASE * db) {
  EMPLOYEE *curEmployee = NULL;
  while ( db->head != NULL ) {
    curEmployee = db->head;
    db->head = db->head->nextEmployee;
    freeEmployee(curEmployee);
  }

  free(db);

}

/* here is where the data file is read
   and generates an index file based on the header */
void genIndex () {
  FILE *dataFile = fopen("database.txt", "r");
  if ( dataFile == NULL) {
    printf("No data file found\n");
    return;
  }

  char *buffer = malloc(sizeof(char)* 201);
  char *indexName = malloc(sizeof(char)* 201);
  int numFields = 0;
  int numRecords = 0;
  
  fgets(buffer, 200, dataFile);
  // Index file must be 5 letters in this implementation
  sscanf(buffer, "IDF=%5s|NREC=%d|NFLD=%d|%*s",indexName, &numRecords, &numFields);

  FILE *indexFile = fopen(indexName,"w");
  int bytePosition = 0;
  int foundFields = 0;
  int foundRecords = 0;
  
  /******************
    Finding starting locations of each record
  *******************/
  fprintf(indexFile,"%d ", bytePosition);
  char letter = '\n';

  // Finds each field and starting location of next record
  while ( (letter = fgetc (dataFile)) != EOF) {
    bytePosition++;
    if ( letter == '|' ) {
      foundFields++;

      // Checks if a new record is found yet and if there is a record after the deliminator
      if (foundFields == numFields && ((numRecords-1) > foundRecords) ) {
        foundRecords ++;
        foundFields = 0;
        // Write location record
        fprintf(indexFile,"%d ", bytePosition);
      }
    }
  }
  
  free (buffer);
  free(indexName);
  fclose(dataFile);
  fclose(indexFile);
}

int main ( int argc, char *args []) {
  // Initialization of main "data file"
  DATABASE *db = malloc( sizeof(DATABASE) );
  db->numEmployees = 0;
  db->numFields = 6;
  db->head = NULL;
  
  // Read the test records in from file (no missing fields)
  FILE *fp = fopen("input.txt", "r");
  if (fp == NULL) {
    return 1;
  }
  
  FILE *outFile = fopen("database.txt","w");
  if (outFile == NULL) {
    return 1;
  }

  char *buffer = malloc( sizeof(char)* 201 );
  int buffer_size = 200;
  int fieldCounter = LNAME;
  db->head = createEmployee();
  EMPLOYEE *curEmployee = db->head;
  
  // Read the file for records & fields
  while ( fgets(buffer, buffer_size, fp) ) {

    // Remove trailing newline
    if ( buffer[ strlen(buffer) - 1  ] == '\n' ) {
      buffer[ strlen(buffer)-1 ] = '\0';
    }
  
    insertField ( curEmployee, buffer, fieldCounter );

    // New record, reset starting field
    if (fieldCounter == POSTAL) {
      fieldCounter = LNAME;
      db->numEmployees++;
      curEmployee->nextEmployee = createEmployee();
      curEmployee = curEmployee->nextEmployee;
    } else {
        fieldCounter++;
    }
  }

  // Print header
  fprintf(outFile,"IDF=index|");
  fprintf(outFile,"NREC=%d|", db->numEmployees);
  fprintf(outFile,"NFLD=%d|", db->numFields);
  fprintf(outFile,"FIELDS=Last_Name|First_Name|Street_Address|City_Name|Province|"
                  "Postal_Code|\n");
                  
  // Print all the records
  curEmployee = db->head;
  while (curEmployee->nextEmployee != NULL) {
    printEmployee( curEmployee, outFile );
    curEmployee = curEmployee->nextEmployee;
  }
  
  // Free any last pieces of memory usage
  fclose(fp);
  fclose(outFile);
  free(buffer);
  freeDatabase(db);
  
  // Generate an index file for the database
  genIndex();
  return 0;
}
